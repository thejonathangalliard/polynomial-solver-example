﻿using PolynomialSolverLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolynomialSolverConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Quadratic quad1 = new Quadratic(1, 3, 2);
            foreach (double result in quad1.SolveForX())
            {
                Console.WriteLine(result);
            }
            Console.ReadKey();
        }
    }
}
