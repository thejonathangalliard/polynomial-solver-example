﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PolynomialSolverLibrary;
using System.Collections.Generic;
using Moq;

namespace PolynomialSolverUnitTests
{
    [TestClass]
    public class QuadraticUnitTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void InvalidQuadraticTest()
        {
            // Arrange & Act // (instantiate object)
            Quadratic quad = new Quadratic(0, 0, 0);
            // Assert // (passes if exception of type ArgumentException thrown)
        }

        [TestMethod]
        public void DiscriminantRealRootTest()
        {
            // Arrange //
            Quadratic quad1 = new Quadratic(1, -2, 3);  // x^2 - 2x + 3x = 0
            Quadratic quad2 = new Quadratic(1, 2, -3);  // x^2 + 2x - 3x = 0
            Quadratic quad3 = new Quadratic(1, 5, 6.25); // x^2 + 5x + 6.25 = 0

            // Act //
            double discriminant1 = quad1.Discriminant; // (-2)^2 - (4 * 1 * 3) = -8
            int realRootCount1 = quad1.RealRootCount; // discriminant is negative so 0 real roots
            int solutionsCount1 = quad1.SolveForX().Count; // number of solutions should equal number of roots (0)

            double discriminant2 = quad2.Discriminant; // (2)^2 - (4 * 1 * -3) = 16
            double realRootCount2 = quad2.RealRootCount; // discriminant is positive so 2 real roots
            int solutionsCount2 = quad2.SolveForX().Count; // number of solutions should equal number of roots (2)

            double discriminant3 = quad3.Discriminant; // (5)^2 - (4 * 1 * 6.25) = 0 
            double realRootCount3 = quad3.RealRootCount; // discriminant = 0 so 1 real root
            double solutionsCount3 = quad3.SolveForX().Count; // number of solutions should equal number of roots (1)
            // Assert //
            Assert.AreEqual(-8, discriminant1);
            Assert.AreEqual(0, realRootCount1);
            Assert.AreEqual(0, solutionsCount1);

            Assert.AreEqual(16, discriminant2);
            Assert.AreEqual(2, realRootCount2);
            Assert.AreEqual(2, solutionsCount2);

            Assert.AreEqual(0, discriminant3);
            Assert.AreEqual(1, realRootCount3);
            Assert.AreEqual(1, solutionsCount3);
        }

        [TestMethod]
        public void SolveForXTest()
        {
            // Arrage //
            Quadratic quad1 = new Quadratic(1, 0, 0);   // Known Quadratic 1 = x^ = 0 -> x = 0 
            Quadratic quad2 = new Quadratic(2, 4, -2);  // Known Quadratic 2x^2 + 4x − 2 = 0 -> x = -1 + sqrt(2), x = -1 - sqrt(2)
            Quadratic quad3 = new Quadratic(-1, -12, -43.21);  // Known Quadratic −x^2 − 12x − 43.21 = 0 -> no real solutions

            // Act //
            List<double> solutions1 = quad1.SolveForX();
            List<double> solutions2 = quad2.SolveForX();
            List<double> solutions3 = quad3.SolveForX();

            // Assert //
            Assert.AreEqual(0, solutions1[0]);

            Assert.AreEqual((-1) + Math.Sqrt(2), solutions2[0]);
            Assert.AreEqual((-1) - Math.Sqrt(2), solutions2[1]);
            Assert.AreNotEqual(solutions2[0], solutions2[1]);

            Assert.AreEqual(0, solutions3.Count);
        }

        [TestMethod]
        public void MagicMoqNonsenseTest()
        {
            // Arrange // 
            Quadratic quad1 = new Quadratic(1, 3, 2);
            var mockPolynomialFactoriser1 = new Mock<PolynomialSolverHelperService>();
            mockPolynomialFactoriser1.Setup(x => x.IsMagical(Moq.It.IsAny<List<double>>())).Returns(true);

            Quadratic quad2 = new Quadratic(1, 3, 2);
            var mockPolynomialFactoriser2 = new Mock<PolynomialSolverHelperService>();
            mockPolynomialFactoriser2.Setup(x => x.IsMagical(Moq.It.IsAny<List<double>>())).Returns(false);

            // Act // 
            string result1 = quad1.MagicMoqNonsense(mockPolynomialFactoriser1.Object);
            string result2 = quad2.MagicMoqNonsense(mockPolynomialFactoriser2.Object);
            
            // Assert // 
            Assert.AreEqual("magic", result1);
            Assert.AreEqual("not magic", result2);
        }
    }
}
