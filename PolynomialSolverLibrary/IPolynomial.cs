﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolynomialSolverLibrary
{
    public interface IPolynomial
    {
        List<double> SolveForX();
    }
}
