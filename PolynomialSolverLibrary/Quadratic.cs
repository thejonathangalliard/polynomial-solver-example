﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialSolverLibrary
{
    public class Quadratic : IPolynomial
    {
        /// <summary>
        /// Coefficient "a" that modifies the x to the power 2 term
        /// </summary>
        public readonly double A;
        /// <summary>
        /// Coefficient "b" that modifies the x to the power 1 term
        /// </summary>
        public readonly double B;
        /// <summary>
        /// Coefficient "c" that modifies the x to the power 0 term
        /// </summary>
        public readonly double C;


        /// <summary>
        /// Discriminant of quadratic indicates number of real roots of quadratic equation
        /// </summary>
        /// <seealso cref="RealRootCount"/>
        public double Discriminant
        {
            get
            {
                return Math.Pow(B, 2) - 4 * A * C;
            }
        }
        /// <summary>
        /// Count of real roots (between 0 and 2) the quadratic equation has
        /// </summary>
        public int RealRootCount
        {
            get
            {
                double discriminant = Discriminant;
                if (discriminant > 0)
                {
                    return 2;
                }
                else if (discriminant == 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Creates a new instance of a quadratic equation in the form ax^2 + bx + c = 0
        /// </summary>
        /// <param name="A">Coefficient "a" that modifies the x to the power 2 term</param>
        /// <param name="B">Coefficient "b" that modifies the x to the power 1 term</param>
        /// <param name="C">Coefficient "c" that modifies the x to the power 0 term</param>
        public Quadratic(double A, double B, double C)
        {
            if (A == 0)
            {
                throw new ArgumentException("Coefficient A can not equal 0, this is not a valid quadratic");
            }

            this.A = A;
            this.B = B;
            this.C = C;
        }

        /// <summary>
        /// Returns an array (2, 1 or 0 results) containing the real solutions for x-intersect
        /// </summary>
        public List<double> SolveForX()
        {
            List<double> results = new List<double>(); // quadratic has maximum 2 solutions 

            if (RealRootCount > 0)
            {
                results = new List<double>(2);
                // Standard quadratic equation for adding the square root of the discriminant
                double quadraticPositive = ((-B) + Math.Sqrt(Discriminant)) / (2 * A);
                results.Add(quadraticPositive);

                // if real root count is 1 this next step is redundant
                if (RealRootCount > 1)
                {
                    // Standard quadratic equation for subracting the square root of the discriminant
                    double quadraticNegative = ((-B) - Math.Sqrt(Discriminant)) / (2 * A);
                    results.Add(quadraticNegative);
                }
            }
            return results;
        }

        /// <summary>
        /// Just a method to demonstrate testing output of method where we moq a dependency
        /// </summary>
        /// <param name="pfs">Dependency injection for handling work of method (used for Moq demo)</param>
        /// 
        public string MagicMoqNonsense(PolynomialSolverHelperService pfs)
        {
            if (pfs.IsMagical(this.SolveForX()))
            {
                return "magic";
            }
            else
            {
                return "not magic";
            }
        }
    }
}
